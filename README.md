# ICRT

ICRT (Industrial Collaborative Robot-human Task) dataset involves the use of four devices for the interaction
between human operator and a collaborative robot. The sensors used are a Leap Motion device (LeapMotion,US), an
ArUco marker [19], an Intel RealSense D415(Intel, US) camera and a Panda robot (Franka Emika, Germany). We
decided to simulate an environment containing multiple sensors as in a typical I4 context

Please do not hesitate to contact us. We will send you the link to the mirror site for the download.
giovanni.menegozzo@univr.it